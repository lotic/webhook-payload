Webhook Contract
================
Your webhook should be a publicly-accessible HTTP endpoint that responds to a
`PUT` request in the format outlined below. Some request headers are omitted
for clarity, such as `Content-Length`.

Request Format
--------------
Fields delimited by `<>` in the JSON payload correspond to values provided at
runtime by the integration framework. The `source.type` key is always the
literal value "com.loticlabs.entities.dataset".

.. code-block
   :linenos:

    PUT /your/endpoint/url HTTP/1.1
    Content-Type: application/json
    X-Lotic-Webhook-Version: v1
    X-Lotic-Event-ID: <event_id>

Request Body
^^^^^^^^^^^^
.. literalinclude:: ../../tests/data/payload.json
    :linenos:
    :language: json

Response Format
---------------
Response should be `PUT` in CSV format with two columns: timestamp and numeric
value.