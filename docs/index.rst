Lotic Webhook Payload API
=========================
This repository contains basic parsing and validation utilities for Lotic
webhook payloads. It requires Python 3.7.1 or greater.

.. toctree::
   :maxdepth: 2
   :caption: Architecture

   arch/webhook_lifecycle
   arch/webhook_contract

.. toctree::
   :maxdepth: 1
   :caption: Classes
   :name: classes
   :glob:

   classes/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
