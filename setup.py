#!/usr/bin/python3
from setuptools import setup

setup(
    python_requires='>=3.7.0',
    setup_requires=['pbr>=1.9', 'setuptools>=17.1'],
    pbr=True,
)
