import json
import logging
import argparse
import pprint

from .payload import Payload


def main():
    """
    Simple utility to test the parsing of a payload JSON.

    python3 -m payload <PATH>
    """
    class CliArgs:
        path = None
    
    # Configure logging
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    # Args namespace
    cli = CliArgs()

    # Parse the command line arguments
    parser = argparse.ArgumentParser(description='Webhook Payload Utility')
    parser.add_argument('path', help='the event payload JSON object')
    args = parser.parse_args(namespace=cli)

    # Open the payload as JSON
    with open(cli.path, 'r') as f:
        payload = json.load(f)
    
    def print_section_title(title):
        """
        Print the title for the section of the payload being displayed.
        """
        print(f'┏━━━━━━━━━━━━┓\n┃{title: ^{12}}┃\n┗━━━━━━━━━━━━┛\n')

    # Validate and parse the payload
    try:
        SOURCES_TITLE = '/sources'
        TARGETS_TITLE = '/targets'
        pp = pprint.PrettyPrinter()
        payload = Payload(**payload)
        print_section_title('Payload')
        pp.pprint(payload)
        print_section_title(SOURCES_TITLE)
        pp.pprint(payload.sources.datasets)
        print()
        print_section_title(TARGETS_TITLE)
        for key, value in payload.targets.series.items():
            print(key)
            pp.pprint(value)
    except Exception as e:
        logging.error('invalid payload: %s', e)

if __name__ == '__main__':
    main()
