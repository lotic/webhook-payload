"""
This module provides support classes and entities useful for deserializing a
JSON webook payload into a Python :py:class:`Payload` object with certain
runtime guarantees and type checking.

.. moduleauthor:: Matthew Marshall <mmarshall@loticlabs.com>
"""

from io import StringIO
import re
from os import path
import json
import logging
import jsonschema

# Regular expression used to define entity types
ENTITY_REGEX = re.compile('^com\.loticlabs\.entities(\.[a-zA-Z]+\w*)+$')

# Logger for payload objects
LOG = logging.getLogger('Payload')


class PayloadObjectBase:
    """
    Base object for payload entities and nested types, provides serialization
    to JSON for ease in debugging and testing.
    """
    def __repr__(self):
        return self._to_json(indent=2)

    def _to_json(self, **kwargs):
        def to_json(obj):
            if isinstance(obj, PayloadObjectBase):
                return obj.__dict__
            else:
                raise ValueError(
                    'cannot serialize type %s: %s' % (type(obj), str(obj))
                )
        return json.dumps(self.__dict__, **kwargs, default=to_json)


class Entity(PayloadObjectBase):
    """
    Base entity class.

    :ivar uuid: the unique identifier of the entity
    :vartype uuid: :py:class:`str`

    :ivar name: an optional human-readable name for the entity
    :vartype name: :py:class:`str`
    """

    def __init__(self, **kwargs):
        """
        Initialize new ``Entity`` with JSON.
        """
        self.uuid = str(kwargs['uuid'])
        self.name = str(kwargs.get('name', None))

    def __str__(self):
        return '[{}] {}'.format(self.uuid, self.name)
    

class Dataset(Entity):
    """
    A Dataset entity, which is usually a source for the webhook event.

    :ivar args: a dictionary of string key/value pairs for this webhook
    :vartype args: dict of :py:class:`str`

    :ivar artifacts: a dictionary of artifact keys to artifacts
    :vartype artifacts: dict of :py:class:`Artifact`
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.args = kwargs['args']
        self.items = EntitySet(**kwargs['items'])

    def __getitem__(self, item):
        return self.items.__getitem__(item)

    @property
    def artifacts(self):
        return self['com.loticlabs.entities.artifact']


class Artifact(Entity):
    """
    An Artifact entity, which is owned by a ``Dataset`` and represents a real
    file.

    :ivar url: the URL with which the artifact can be retrieved via `GET`
    :vartype url: :py:class:`str`
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.url = kwargs['url']


class Series(Entity):
    """
    A Series entity, which represents a time series. This is currently the only
    supported target for webhooks.

    :ivar url: the URL to which the HTTP request of the webhook response should be sent
    :vartype url: :py:class:`str`

    :ivar method: `"PUT"`: the HTTP method that should be used in the request
    :vartype method: :py:class:`str`

    :ivar output: metadata about the requested output from the webhook
    :vartype output: :py:class:`Output`
    """
    class Output(PayloadObjectBase):
        """
        Parameters provided to the webhook to inform it of how to respond to the
        request, and what attributes and file format are expected in the HTTP
        `PUT` response.

        :ivar attributes: a list of standard field names that should be returned
        :vartype attributes: list of :py:class:`str`

        :ivar format: the format of the response body that should be returned
        :vartype format: :py:class:`str`

        :ivar csv: additional metadata about the CSV response (if applicable)
        :vartype csv: :py:class:`CsvOptions`
        """
        class CsvOptions(PayloadObjectBase):
            """
            CSV formatting options

            :ivar field_delimiter: the field delimiter to be used by the webhook
            :vartype field_delimiter: :py:class:`str`
            """

            def __init__(self, **kwargs):
                self.field_delimiter = kwargs.get('field_delimiter', None)

        def __init__(self, **kwargs):
            self.attributes = kwargs['attributes']
            self.format = kwargs['format']
            if 'csv' in kwargs:
                self.csv = Series.Output.CsvOptions(**kwargs['csv'])

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.url = kwargs['url']
        self.method = kwargs['method']
        self.output = Series.Output(**kwargs['output'])


ENTITY_MAP = {
    'com.loticlabs.entities.dataset': Dataset,
    'com.loticlabs.entities.artifact': Artifact,
    'com.loticlabs.entities.series': Series,
}


class EntitySet(PayloadObjectBase):
    """
    Base class for the ``sources`` and ``targets`` properties of the payload
    JSON document.

    :cvar counts: a dictionary of the counts of each entity type
    """

    class Counts(PayloadObjectBase):
        """
        Counts list.

        :ivar total: total count of all entities
        :vartype total: :py:class:`int`
        """
        def __init__(self, **kwargs):
            """
            Initialize new ``Counts`` with JSON.
            """
            self._entities = {}
            self.total = int(kwargs['total'])
            for key, value in kwargs.items():
                if key.startswith('com.loticlabs.entities'):
                    self._entities.setdefault(key, int(value))

        def __getitem__(self, entity_name):
            """
            Looks up the count of an entity by name.
            """
            if entity_name == 'total':
                return self.total
            else:
                return self._entities.get(entity_name, 0)
        
        @property
        def __dict__(self):
            return dict(total=self.total, **self._entities)

        def __iter__(self):
            """
            Gets the iterator of counts.
            """
            return self._entities.items().__iter__()

        def __str__(self):
            """
            Get a string representation of the counts.
            """
            return '{} entities'.format(self.total)

    def __init__(self, **kwargs):
        """
        Initialize a new ``EntitySet`` with JSON.
        """
        self._entities = {}
        self.counts = EntitySet.Counts(**kwargs['counts'])
        for key, value in kwargs.items():
            if key == 'counts':
                continue
            if not ENTITY_REGEX.match(key):
                LOG.warning('invalid key for EntitySet: %s', key)
                continue
            type = value.__class__.__name__
            if not isinstance(value, dict) and not isinstance(value, list):
                raise ValueError(
                    'value for %s must be a dict or list, not a %s' % (key, type))
            entity = ENTITY_MAP.get(key, None)
            if not entity:
                LOG.warning('entity %s not found, storing as %s', key, type)
                self._entities[key] = value
            else:
                if not callable(entity):
                    raise ValueError('%s is not callable' % (entity,))
                if isinstance(value, dict):
                    # This is an entity map
                    entities = {}
                    for k, v in value.items():
                        entities[k] = entity(**v)
                    self._entities[key] = entities
                elif isinstance(value, list):
                    self._entities[key] = list(
                        map(lambda x: entity(**x), value))

    def __getitem__(self, name):
        """
        Get the entities of a certain name.
        """
        return self._entities.get(name)
    
    @property
    def __dict__(self):
        return dict(counts=self.counts,**self._entities)


class Payload(PayloadObjectBase):
    """
    A :py:class:`Payload` encapsulates the data for an incoming webhook request
    that can be handled by a Lotic webhook implementation.
    
    This is a JSON document that gets `POST`ed to the webhook URL and contains
    necessary details about the inputs and outputs of the transformation to be
    performed by the webhook.

    A payload consists of two parts:
    
    #. `sources` of data (list of :py:class:`Dataset`) contained within a
       :py:class:`SourceSet`, and
    #. `targets` of data transformation (map of :py:class:`Series`) contained
       within a :py:class:`TargetSet`

    It is the responsibility of the webhook implementor to process the payload,
    inspect the `sources`, and `PUT` data as needed to the `targets`. The
    process is asynchronous, so the webhook should respond with a `2XX` HTTP
    status code to indicate successful operation, even if it has not `PUT` a
    response to each :py:class:`Series` in the `targets` list.

    :ivar sources: the sources of this webhook event
    :vartype sources: :py:class:`SourceSet`

    :ivar targets: the targets of this webhook event
    :vartype targets: :py:class:`TargetSet`

    :Example:
    .. code-block:: python

       # Read the payload as JSON
       payload = json.load(payload_str)

       # Validate and parse the payload
       try:
           print(str(payload))
           print('Sources:')
           for dataset in payload.sources.datasets:
               print(dataset)
           print()
           print('Targets:')
           for key, value in payload.targets.artifacts.items():
               print('{} => {}', key, value)
       except Exception as e:
           logging.error('invalid payload: %s', e)
    """

    class Validator:
        """
        A simple wrapper for the ``jsonschema`` Python module that loads the
        embedded JSON schema in this this repository.

        :ivar schema: the JSON schema used to validate the payload
        :vartype schema: :py:class:`object`

        :Example:
        .. code-block:: python

           validate = Payload.Validator()
           validate(payload)
        """

        def __init__(self):
            """
            Initialize a new ``Validator``.
            """
            schema_path = path.abspath(
                path.join(
                    path.dirname(__file__),
                    'schema/payload.json',
                )
            )
            with open(schema_path, 'r') as f:
                self.schema = json.load(f)

        def __call__(self, payload, *args, **kwargs):
            """
            Validate the payload
            """
            jsonschema.validate(payload, self.schema)

    class SourceSet(EntitySet):
        """
        The ``sources`` list from the payload.

        :ivar datasets: datasets that are sources for this webhook event
        :vartype datasets: list of :py:class:`Dataset`
        """

        def __init__(self, **kwargs):
            super().__init__(**kwargs)

        @property
        def datasets(self):
            return list(self['com.loticlabs.entities.dataset'])

    class TargetSet(EntitySet):
        """
        The ``targets`` list from the payload.

        :ivar series: a dictionary of target keys to target series
        :vartype series: dict of :py:class:`Series`
        """

        def __init__(self, **kwargs):
            super().__init__(**kwargs)

        @property
        def series(self):
            return self['com.loticlabs.entities.series']

    def __init__(self, *args, **kwargs):
        """
        Initialize a new webhook payload.
        """
        Payload.Validator()(kwargs)
        self.sources = Payload.SourceSet(**kwargs['sources'])
        self.targets = Payload.TargetSet(**kwargs['targets'])

    def __str__(self):
        return 'Payload[sources={}, targets={}]'.format(
            self.sources.counts.total,
            self.targets.counts.total,
        )
