Lotic Webhook Payload
=====================
|CircleCI|

This repository contains basic parsing and validation utilities for Lotic
webhook payloads. It requires Python 3.7.1 or greater.

Documentation
-------------

Python (Sphinx-generated) documentation is available here:

https://storage.googleapis.com/lotic-docs/webhook-payload/index.html

Install
-------
This package can be installed directly from `PyPi <https://pypi.org/>`_.

.. code-block:: shell

   pip install loticlabs-webhook-payload

Test
----
This repository contains a basic test function that parses a webhook payload and
prints the information to the console.

.. code-block:: shell

   python3 -m unittest

.. Badges
.. |CircleCI| image:: https://circleci.com/bb/lotic/webhook-payload.svg?style=svg
   :target: https://circleci.com/bb/lotic/webhook-payload
