import json
import unittest
import logging
from os import path

from lotic.webhook.payload import Payload


class BasicPayloadTests(unittest.TestCase):
    """
    Basic tests for parsing a payload JSON document.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        logging.basicConfig()
        logging.getLogger().setLevel(logging.INFO)
        self.LOG = logging.getLogger(self.__class__.__name__)

    def get_sample_payload_json(self):
        """
        Load the sample payload as a JSON document.
        """

        # Get the sample payload path
        file = path.abspath(
            path.join(
                path.dirname(__file__),
                'data/payload.json',
            )
        )

        # Open the payload as JSON
        self.LOG.info('loading JSON from %s', file)
        with open(file, 'r') as f:
            payload_json = json.load(f)

        # Return the **unvalidated** payload read in as a JSON document
        return payload_json

    def test_parse_payload(self):

        # Parse JSON payload
        payload = self.get_sample_payload_json()
        payload = Payload(**payload)

        # Validate "sources"
        self.assertEqual(1, payload.sources.counts.total)
        self.assertEqual(1, payload.sources.counts['com.loticlabs.entities.dataset'])
        self.assertEqual(1, len(payload.sources.datasets))

        ds = payload.sources.datasets[0]
        self.assertEqual('3f45d48d-0184-43d4-a84d-7ec6defcd38a', ds.uuid)
        self.assertEqual('Sample Dataset', ds.name)
        self.assertEqual(ds.args['dataset_target_arg_key'], 'dataset_target_arg_value')

        items = ds.items
        self.assertEqual(1, items.counts.total)
        self.assertEqual(1, items.counts['com.loticlabs.entities.artifact'])

        self.assertEqual(1, len(ds.artifacts))
        artifact = ds.artifacts['sample_artifact_key']
        self.assertEqual('9876', artifact.uuid)
        self.assertEqual('Sample Artifact', artifact.name)
        self.assertEqual('https://httpbin.org/json', artifact.url)

        # Validate "targets"
        self.assertEqual(1, payload.targets.counts.total)
        self.assertEqual(1, payload.targets.counts['com.loticlabs.entities.series'])
        self.assertEqual(1, len(payload.targets.series))

        series = payload.targets.series['sample_target_key']
        self.assertEqual('b8e8a501-6789-4200-8fbc-10345cc51875', series.uuid)
        self.assertEqual('https://httpbin.org/post', series.url)
        self.assertEqual('PUT', series.method)

        output = series.output
        attributes = output.attributes
        self.assertIn('com.loticlabs.field.timestamp', attributes)
        self.assertIn('com.loticlabs.field.numeric_value', attributes)
        self.assertEqual('csv', output.format)
        csv = output.csv
        self.assertEqual('\t', csv.field_delimiter)
